v0.5
-----

Release date: not yet.

The package has been divided in several subpackages.

* The subpackage ``` MultiSlabs`MultiSlabs` ``` contains common parameters
  and functions to be shared among other subpackages.

* The subpackage ``` MultiSlabs`KronigPenney` ``` contains functions to
  find the energy of a particle subject to a Kronig-Penney potential.

* The subpackage ``` MultiSlabs`Infinite`Bosons` ``` contains subpackages
  to calculate the thermodynamic properties of bosons in an infinite periodic
  system of multi-slabs.



v0.1
-----

Initial commit. No code :(
