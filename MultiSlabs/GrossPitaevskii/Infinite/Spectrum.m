(* Mathematica Package         *)
(* Created by IntelliJ IDEA    *)

(* :Title: Spectrum     *)
(* :Context: Spectrum`  *)
(* :Author: Omar            *)
(* :Date: 12/03/2015              *)

(* :Package Version: 1.0       *)
(* :Mathematica Version:       *)
(* :Copyright: (c) 2015 Omar *)
(* :Keywords:                  *)
(* :Discussion:                *)

BeginPackage["Spectrum`"]
(* Exported symbols added here with SymbolName::usage *)

SystemBoundaryConditionsSolver::usage = "SystemBoundaryConditionsSolver[u0, a, b, g0] finds the chemical potential of the system";

Begin["`Private`"] (* Begin Private Context *)

Get["MultiSlabs`MultiSlabs`"];


(* Returns the density as a function of Jacobi elliptic sine function *)
SetAttributes[Density, Listable];
SetAttributes[Density, NumericFunction];
Density[g0n0_, roff_, sql_, m_, zoff_, z_] := roff + Re[4 m sql JacobiSN[Sqrt[2 g0n0] Sqrt[sql] (z - zoff), m]^2];


(* Returns the integral of the square of the Jacbine elliptic squared sine *)
SetAttributes[JacobiIntegralSn, Listable];
SetAttributes[JacobiIntegralSn, NumericFunction];
JacobiIntegralSn[g0n0_, sql_, m_, zoff_, z_] := (z - zoff) / m -
    Re[EllipticE[JacobiAmplitude[Sqrt[2 g0n0] Sqrt[sql] (z - zoff), m], m] / (m Sqrt[2 g0n0] Sqrt[sql])];


(* Returns the derivative of the density *)
SetAttributes[DensityDerivative, Listable];
SetAttributes[DensityDerivative, NumericFunction];
DensityDerivative[g0n0_, sql_, m_, zoff_, z_] := Re[2 Sqrt[2 g0n0] Sqrt[sql] (4 m sql) JacobiSN[Sqrt[2 g0n0] Sqrt[sql] (z - zoff), m] 	JacobiCN[Sqrt[2 g0n0] Sqrt[sql] (z - zoff), m] JacobiDN[Sqrt[2 g0n0] Sqrt[sql] (z - zoff), m]];


(* Returns the indefinite integral of the density *)
SetAttributes[DensityIntegral, Listable];
SetAttributes[DensityIntegral, NumericFunction];
DensityIntegral[g0n0_, roff_, sql_, m_, zoff_, z_] := roff z + 4 m sql JacobiIntegralSn[g0n0, sql, m, zoff, z];


BoundaryConditionsEquations[
	VExtParams_, InteractionParams_, r1off_, sql1_, m1_, z1off_, r2off_, sql2_, m2_, z2off_
] := Module[{u0, g0n0, r, a0, r1zero, r2zero, Dr1zero, Dr2zero, r1a, r2minusb, Dr1a, Dr2minusb, Dr1ah, Dr2minusbh,
	xr1off, xsql1, xm1, xz1off, xr2off, xsql2, xm2, xz2off, xmu1, xmu2, xgm1, xgm2, xsqa1, xsqa2,
	n1, n2, wp, pg, ag, fn1, fn2, fn3, fn4, fn5, fn6, fn7, fn8},

	(*{wp, pg, ag} = EstimatePrecisions @@ {r1off, sql1, m1, z1off, r2off, sql2, m2, z2off};*)
	{u0, r, a0} = VExtParams;
	g0n0 = Times @@ (InteractionParams);
	{xr1off, xsql1, xm1, xz1off} = {r1off, sql1, m1, z1off};
	{xr2off, xsql2, xm2, xz2off} = {r2off, sql2, m2, z2off};

	(*{u0, r, a0} = SetPrecision[#, Infinity] &/@ VExtParams;*)
	(*g0n0 = Times @@ (SetPrecision[#, Infinity] &/@ InteractionParams);*)
	(*{xr1off, xsql1, xm1, xz1off} = SetPrecision[#, Infinity] &/@ {r1off, sql1, m1, z1off};*)
	(*{xr2off, xsql2, xm2, xz2off} = SetPrecision[#, Infinity] &/@ {r2off, sql2, m2, z2off};*)

	(* Density for region 1 and 2 evaluated at the origin *)
	r1zero = Density[g0n0, xr1off, xsql1, xm1, xz1off, 0];
	r2zero = Density[g0n0, xr2off, xsql2, xm2, xz2off, 0];

	(* Density for region 1 and 2 evaluated at the extremes of the period of the potential. *)
	r1a = Density[g0n0, xr1off, xsql1, xm1, xz1off, 1 / (1 + r)];
	r2minusb = Density[g0n0, xr2off, xsql2, xm2, xz2off, -r / (1 + r)];

	(* Derivative of the density for regio	n 1 and 2 evaluated at the origin *)
	Dr1zero = DensityDerivative[g0n0, xsql1, xm1, xz1off, 0];
	Dr2zero = DensityDerivative[g0n0, xsql2, xm2, xz2off, 0];

	(* Derivative of the density for region 1 and 2 evaluated at the extremes of the period of the potential. *)
	Dr1a = DensityDerivative[g0n0, xsql1, xm1, xz1off, 1 / (1 + r)];
	Dr2minusb = DensityDerivative[g0n0, xsql2, xm2, xz2off, -r / (1 + r)];

	Dr1ah = DensityDerivative[g0n0, xsql1, xm1, xz1off, (1 / 2) 1 / (1 + r)];
	Dr2minusbh = DensityDerivative[g0n0, xsql2, xm2, xz2off, (-r / 2) 1 / (1 + r)];

	n1 = Subtract @@ DensityIntegral[g0n0, xr1off, xsql1, xm1, xz1off, {1 / (1 + r), 0}];
	n2 = Subtract @@ DensityIntegral[g0n0, xr2off, xsql2, xm2, xz2off, {0, - r / (1 + r)}];

	(*xmu1 = g0 n0 (3 xr10 + 4 xsql1 (xm1 + 1)) / 2;*)
	(*xmu2 = 2 u0 - g0 n0 (3 xr20 + 4 xsql2 (xm2 + 1))/2;*)

	xmu1 = g0n0 (3 xr1off + 4 xsql1 (xm1 + 1)) / 2;
	xmu2 = u0 + g0n0 (3 xr2off + 4 xsql2 (xm2 + 1)) / 2;

	(*xgm1 = -2 g0n0 (3 xr1off^2 + 8 xsql1 (xm1 + 1) xr1off + 16 xm1 xsql1^2);*)
	(*xgm2 = -2 g0n0 (3 xr2off^2 + 8 xsql2 (xm2 + 1) xr2off + 16 xm2 xsql2^2);*)

	(*xsqa1 = g0n0 xr1off (xr1off + 4 xsql1) (xr1off + 4 xsql1 xm1) / 2;*)
	(*xsqa2 = g0n0 xr2off (xr2off + 4 xsql2) (xr2off + 4 xsql2 xm2) / 2;*)
	xsqa1 = g0n0 xr1off (xr1off + 4 xsql1) (xr1off + 4 xsql1 xm1) / 2;
	xsqa2 = g0n0 xr2off (xr2off + 4 xsql2) (xr2off + 4 xsql2 xm2) / 2;

	fn1 = r1zero - r2zero;																(* Continuity of the density *)
	fn2 = Dr1zero - Dr2zero;
    fn3 = Dr1ah;
    (*fn5 = r1a - r2minusb;*)
	fn4 = Dr2minusbh;
	(*fn5 = Dr1a - Dr2minusb;*) 		(* XXX: Not sure if this is needed... *)
    fn6 = xmu1 - xmu2;
    fn7 = xsqa1;
	fn5 = xsqa2;
    fn8 = n1 + n2 - 1;

	(*Print[{fn1, fn2, fn3, fn4, fn5, fn6, fn7, fn8}];*)

	(**************************************************************************************
		The set of eight nonlinear equations that determine the solution of the system.
	***************************************************************************************)
	Return[SetPrecision[#, Infinity] &/@ {fn1, fn2, fn3, fn4, fn5, fn6, fn7, fn8}];
]


SystemBoundaryConditionsSolver[u0_, r_, a0_, g0n0_, {wp_, pg_, ag_}, mi_:20, ic_:False] := Module[
	{PCR, NLSE, cubicCoefficientsValley, cubicCoefficientsHill, cubicZerosValley,
		cubicZerosHill, r10, sql1, m1, r20, sql2, m2, $Jacobian, Jac, x, x0, ShowParams, rv, k,
		$r1off, $sql1, $m1, $z1off, $r2off, $sql2, $m2, $z2off, $m1TimesSql1, $m2TimesSql2},

	NLSE[x_?VectorQ] := BoundaryConditionsEquations[
		{u0, r, a0}, {g0n0}, x[[1]], x[[2]], x[[3]], x[[4]], x[[5]], x[[6]], x[[7]], x[[8]]
	];

	ShowParams[step_, x_, eqns_] := Module[{r1Params, r2Params},
		r1Params = x[[;;4]];
		r2Params = x[[5;;]];

		Print[ToString@StringForm["****** Step `1` ******", step]];
		Print[TableForm[
			Transpose[N[{r1Params, r2Params}, MachinePrecision]],
			(*{{r10, r20}, {sql1, sql2}, {m1, m2}, {z1off, z2off}},*)
			TableHeadings -> {{"roff", "sql", "m", "zoff"}, {"Region 1", "Region 2"}}
		]];
		Print[TableForm[
			{N[eqns, MachinePrecision]},
			TableHeadings -> {None, {"f1", "f2", "f3", "f4", "f5", "f6", "f7", "f8"}}
		]];
	];

	(*SetPrecision[#, Infinity] &/@*) (x /. Module[{},

		$m1TimesSql1 = 2;
		$m2TimesSql2 = -2;

		k = 0;
		x0 = SetPrecision[#, Infinity] &/@ If[ic === False, {1/2, Pi^2, 1, 1/2, 1, -1, 1/2, -1/2}, ic];

		(*ShowParams[k, x0, NLSE[x0]];*)
		(*Print[MatrixForm@N[Jac[x0], MachinePrecision]];*)

		(* The return value *)
		FindRoot[NLSE[x] == 0, {x, x0},
			WorkingPrecision -> wp,
			PrecisionGoal -> 3,
			AccuracyGoal -> 3,
			Method -> {"Newton", "StepControl" -> {"TrustRegion"}},
			MaxIterations -> mi,
			(*Jacobian -> Jac[x],*)
			StepMonitor :> (If[TrueQ[$DEBUG],
				k = k + 1;
				ShowParams[k, x, NLSE[x]];
				(*Print[MatrixForm@N[Jac[x], MachinePrecision]];*)
			];)
		]]
	)
];


(* Find the energy of a particle as function of the momentum for the given
parameters for the potential u0 and r *)
(*ClearAll[Density];*)
(*SetAttributes[Density, Listable];*)
(*SetAttributes[Density, NumericFunction];*)
(*Density[u0_?NumericQ, a_?NumericQ, b_?NumericQ, g0_?NumericQ] /; If[u0 == 0 || b == 0, True,*)
(*If[InexactNumberQ[u0] || InexactNumberQ[a] || InexactNumberQ[b] || InexactNumberQ[g0],*)
(*True, False]*)
(*] := Block[{RDR, DRDR, xu0, xr, xkS, energies, ej, wp, pg, ag, j, rv},*)

(* Trivial case *)
(*If[u0 == 0 || b == 0, Return["XXX"]]; *)(* Exit *)

(*rv = $EnergyCache[u0, r, kS];*)
(*If[NumericQ[rv], Return[rv]]; *)(* Exit now. *)

(*{wp, pg, ag} = 2 EstimatePrecisions[u0, a, b, g0];*)
(*j = 1 + Quotient[Abs[kS], Pi];*)
(*energies = MachineCentralEnergies[u0, r, j];*)

(*ej = If[j <= Length[energies],*)
(*{{ energies[[j]] }},*)
(*********************************************************************)

(*For this case the "far band" approximation is valid, and the*)
(*energy for the kS momentum is very close to kS^2 + r/(1+r) u0*)

(*********************************************************************)
(*{{ kS^2 + r/(1+r) u0 }}*)
(*];*)
(*rv = (e /. FindRoot[RDR[e], {e, ej},*)
(*WorkingPrecision -> wp, PrecisionGoal -> pg, AccuracyGoal -> ag,*)
(*Jacobian -> DRDR[e]*)
(*])[[1, 1]];*)

(*$EnergyCache[u0, r, kS] = rv;*)
(*Return[rv];*)
(*];*)



End[] (* End Private Context *)

EndPackage[]
