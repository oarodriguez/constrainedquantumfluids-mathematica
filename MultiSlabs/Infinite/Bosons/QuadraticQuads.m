(* :Name: QuadraticQuads.m *)

(* :Title: QuadraticQuads *)

(* :Author: Omar Abel Rodríguez <oarodriguez@live.com.mx> *)

(* :Summary: Implements routines to find the thermodynamic properties of
a system of bosons subject to a Kronig-Penney potential, where their energy
in the transversal direction to the potential barriers is quadratic. *)

(* :Context: MultiSlabs`Infinite`Bosons`QuadraticQuads` *)

(* :Package Version: 1.0 *)

(* :Copyright: Copyright 2014, *)

(* :History: *)

(* :Keywords: Quantum systems *)

(* :Source: *)

(* :Warnings: *)



BeginPackage["MultiSlabs`Infinite`Bosons`QuadraticQuads`"];


Log1dExpInt::usage = "Log1dExpInt[u0, r, a0, T, zd0] calculates the integral for Log[1 - Exp[-\[Gamma](\[Epsilon] - \[Mu])/T]] over interval (0, \[Infinity]).";

Log1dExpPsi2Int::usage = "Log1dExpPsi2Int[u0, r, a0, zr, T, zd0] calculates the integral for Log[1 - Exp[-\[Gamma](\[Epsilon] - \[Mu])/T]]]Abs[\[CapitalPsi][u0, r, kS, zr]]^2 over kS interval (0, \[Infinity]).";

ede0Log1dExpInt::usage = "ede0Log1dExpInt[u0, r, \[Gamma], T, zd0] calculates the integral for (\[Epsilon] - \[Epsilon]0) Log[1 - Exp[-\[Gamma](\[Epsilon] - \[Mu])/T]] over interval (0, \[Infinity]).";

G2ExpInt::usage = "G2ExpInt[u0, r, \[Gamma], T, zd0] calculates the integral for PolyLog[2, Exp[-\[Gamma](\[Epsilon] - \[Mu])/T]] over interval (0, \[Infinity]).";

ede0edmuTDTmuQExpd1Int::usage = "ede0edmuTDTmuQExpd1Int[u0, r, \[Gamma], T, zd0, TDTmu] calculates the integral for (\[Epsilon] - \[Epsilon]0) (\[Epsilon] - \[Mu] + T d\[Mu]/dT) / (Exp[\[Gamma](\[Epsilon] - \[Mu])/T] - 1) over interval (0, \[Infinity]).";

edmuTDTmuLog1dExpInt::usage = "edmuTDTmuLog1dExpInt[u0, r, \[Gamma], T, zd0, TDTmu] calculates the integral for (\[Epsilon] - \[Mu] + T d\[Mu]/dT) Log[1 - Exp[-\[Gamma](\[Epsilon] - \[Mu])/T]] over interval (0, \[Infinity]).";

edmuQExpd1Int::usage = "edmuQExpd1Int[u0, r, \[Gamma], T, zd0] calculates the integral for (\[Epsilon] - \[Mu]) / (Exp[\[Gamma](\[Epsilon] - \[Mu])/T] - 1) over interval (0, \[Infinity]).";

OneQExpd1Int::usage = "OneQExpd1Int[u0, r, \[Gamma], T, zd0] calculates the integral for 1 / (Exp[\[Gamma](\[Epsilon] - \[Mu])/T] - 1) over interval (0, \[Infinity]).";


Begin["`Private`"];


Get["MultiSlabs`MultiSlabs`"];
Get["MultiSlabs`Infinite`Bosons`Functions`"];


Energy = MultiSlabs`Infinite`KronigPenney`Energy;
EnergyDiracComb = MultiSlabs`Infinite`KronigPenney`EnergyDiracComb;
WaveFunction = MultiSlabs`Infinite`KronigPenney`WaveFunction;


Log1dExpInt::PrettyString = "Log[1 - Exp[-\[Gamma](\[Epsilon] - \[Mu])/T]].";
Log1dExpInt[$E_, VParams_, T_, zd0_] := Block[
	{a0, g, ekS0, xa0, xT, xzd0, FirstBandIntegrand, Integrand, wp, pg, ag, eps},

	a0 = Last[VParams];
	{xa0, xT, xzd0} = SetPrecision[#, Infinity]& /@ {a0, T, zd0};
	g = xa0^(-2) / (4 Pi);

	{wp, pg, ag} = EstimatePrecisions @@ Join[VParams, {T, zd0}];
	eps = PrecisionEps[wp]/2;
	Integrand[kS_?NumberQ] := N[Log[1 - xzd0 Exp[-g ($E[kS] - $E[0]) / xT]], wp+1];

	FirstBandIntegrand[kS_?NumberQ] := (
		(***********************************************************************

						FIRST BAND SINGULARITY HANDLING

		Convert kS to an exact number and pass it as argument to the energy
		function, so even if the first band is very narrow we can get a nonzero
		approximation of the difference between momentum kS and ground energy.
		Mathematica increases precision internally to meet goal precisions, and
		even when the result of applying function N to an expression may have a
		some precision, its accuracy remains as high that used for internal
		computations in N. So we pass the momentum kS to subsequent operations
		with a precision as high as its accuracy.

		***********************************************************************)
		ekS0 = N[$E[SetPrecision[kS, Infinity]] - $E[0], wp+1];
		ekS0 = SetPrecision[ekS0, Accuracy@ekS0];
		If[g ekS0 / xT < eps,
			(* Case when exponential argument is very small, even less than the
			epsilon for precision wp. *)
			N[Log[1 - xzd0 + xzd0 g ekS0 / xT], wp+1],

			(* Default case. *)
			N[Log[1 - xzd0 Exp[-g ekS0 / xT]], wp+1]
		]
	);

	Return[
		Integrator[
			Integrand,								(* Base integrand *)
			Log1dExpInt::PrettyString,				(* String name *)
			SetPrecision[#, Infinity] &/@ VParams,	(* Potential parameters *)
			xT,										(* Relative fugacity *)
			xzd0,									(* Temperature *)
			{wp, pg, ag},							(* Precisions *)
			FirstBandIntegrand						(* First band integrand *)
		]
	];
];


Log1dExpPsi2Int::PrettyString =  "Log[1 - Exp[-\[Gamma](\[Epsilon] - \[Mu])/T]]
Abs[\[CapitalPsi][\!\(\*SubscriptBox[\(u\), \(0\)]\), r, k(a+b), z/(a+b)]\!\(\*SuperscriptBox[\(]\), \(2\)]\).";
Log1dExpPsi2Int[$E_, $WF_, VParams_, T_, zd0_] := Block[
	{a0, g, ekS0, xa0, xT, xzd0, FirstBandIntegrand, Integrand, wp, pg, ag, eps},

	a0 = Last[VParams];
	{xa0, xT, xzd0} = SetPrecision[#, Infinity]& /@ {a0, T, zd0};
	g = xa0^(-2) / (4 Pi);

	{wp, pg, ag} = EstimatePrecisions @@ Join[VParams, {T, zd0}];
	eps = PrecisionEps[wp]/2;
	Integrand[kS_?NumberQ] := (
		N[Log[1 - xzd0 Exp[-g ($E[kS] - $E[0]) / xT]] Abs[$WF[kS]]^2, wp+1]
	);

	FirstBandIntegrand[kS_?NumberQ] := (
		(***********************************************************************

						FIRST BAND SINGULARITY HANDLING

		Convert kS to an exact number and pass it as argument to the energy
		function, so even if the first band is very narrow we can get a nonzero
		approximation of the difference between momentum kS and ground energy.
		Mathematica increases precision internally to meet goal precisions, and
		even when the result of applying function N to an expression may have a
		some precision, its accuracy remains as high that used for internal
		computations in N. So we pass the momentum kS to subsequent operations
		with a precision as high as its accuracy.

		***********************************************************************)

		ekS0 = N[$E[SetPrecision[kS, Infinity]] - $E[0], wp+1];
		ekS0 = SetPrecision[ekS0, Accuracy@ekS0];
		If[g ekS0 / xT < eps,
			(* Case when exponential argument is very small, even less than the
			epsilon for precision wp, i.e., the difference between number one and
			the next nearest representable number with precision wp. *)
			N[Log[1 - xzd0 + xzd0 g ekS0 / xT] Abs[$WF[kS]]^2, wp+1],
			N[Log[1 - xzd0 Exp[-g ekS0 / xT]] Abs[$WF[kS]]^2, wp+1]
		]
	);

	Return[
		Integrator[
			Integrand,
			Log1dExpPsi2Int::PrettyString,
			SetPrecision[#, Infinity] &/@ VParams,
			xT,
			xzd0,
			{wp, pg, ag},
			FirstBandIntegrand
		]
	];
];


ede0Log1dExpInt::PrettyString =  "(\[Epsilon] - \[Epsilon]0) Log[1 - Exp[-\[Gamma](\[Epsilon] - \[Mu])/T]].";
ede0Log1dExpInt[$E_, VParams_, T_, zd0_] := Block[
	{a0, g, ekS0, xa0, xT, xzd0, FirstBandIntegrand, Integrand, wp, pg, ag, eps},

	a0 = Last[VParams];
	{xa0, xT, xzd0} = SetPrecision[#, Infinity]& /@ {a0, T, zd0};
	g = xa0^(-2) / (4 Pi);

	{wp, pg, ag} = EstimatePrecisions @@ Join[VParams, {T, zd0}];
	eps = PrecisionEps[wp]/2;
	Integrand[kS_?NumberQ] := N[($E[kS] - $E[0]) Log[1 - xzd0 Exp[-g ($E[kS] - $E[0]) / xT]], wp];

	FirstBandIntegrand[kS_?NumberQ] := (
		ekS0 = N[$E[SetPrecision[kS, Infinity]] - $E[0], wp];
		ekS0 = SetPrecision[ekS0, Accuracy@ekS0];
		If[g ekS0 / xT < eps,
			(* Case when exponential argument is very small, even less than the
			epsilon for precision wp, i.e., the next nearest representable number
			at precision wp. *)
			N[ekS0 Log[1 - xzd0 + xzd0 g ekS0 / xT], wp],

			(* Other cases... *)
			N[ekS0 Log[1 - xzd0 Exp[-g ekS0 / xT]], wp]
		]
	);

	Return[
		Integrator[
			Integrand,
			ede0Log1dExpInt::PrettyString,
			SetPrecision[#, Infinity] &/@ VParams,
			xT,
			xzd0,
			{wp, pg, ag},
			FirstBandIntegrand
		]
	];
];


G2ExpInt::PrettyString =  "PolyLog[2, Exp[-\[Gamma](\[Epsilon] - \[Mu])/T]].";
G2ExpInt[$E_, VParams_, T_, zd0_] := Block[
	{a0, g, ekS0, xa0, xT, xzd0, FirstBandIntegrand, Integrand, wp, pg, ag, eps},

	a0 = Last[VParams];
	{xa0, xT, xzd0} = SetPrecision[#, Infinity]& /@ {a0, T, zd0};
	g = xa0^(-2) / (4 Pi);

	{wp, pg, ag} = EstimatePrecisions @@ Join[VParams, {T, zd0}];
	eps = PrecisionEps[wp]/2;
	Integrand[kS_?NumberQ] := N[PolyLog[2, xzd0 Exp[-g ($E[kS] - $E[0]) / xT]], wp];

	FirstBandIntegrand[kS_?NumberQ] := (
		ekS0 = N[$E[SetPrecision[kS, Infinity]] - $E[0], wp];
		ekS0 = SetPrecision[ekS0, Accuracy@ekS0];
		If[g ekS0 / xT < eps,
			(* Case when exponential argument is very small, even less than the
			epsilon for precision wp, i.e., the next nearest representable number
			at precision wp. *)
			N[PolyLog[2, xzd0 (1 - g ekS0 / xT)], wp],

			(* Other cases... *)
			N[PolyLog[2, xzd0 Exp[-g ekS0 / xT]], wp]
		]
	);

	Return[
		Integrator[
			Integrand,
			G2ExpInt::PrettyString,
			SetPrecision[#, Infinity] &/@ VParams,
			xT,
			xzd0,
			{wp, pg, ag},
			FirstBandIntegrand
		]
	];
];


ede0edmuTDTmuQExpd1Int::PrettyString =  "(\[Epsilon] - \[Epsilon]0) (\[Epsilon] - \[Mu] + T d\[Mu]/dT) / (Exp[\[Gamma](\[Epsilon] - \[Mu])/T] - 1).";
ede0edmuTDTmuQExpd1Int[$E_, VParams_, T_, zd0_, TDTmu_] := Block[
	{a0, g, ekS0, xa0, xT, xzd0, xTDTmu, FirstBandIntegrand, Integrand, wp, pg, ag, eps},

	a0 = Last[VParams];
	{xa0, xT, xzd0, xTDTmu} = SetPrecision[#, Infinity]& /@ {a0, T, zd0, TDTmu};
	g = xa0^(-2) / (4 Pi);

	{wp, pg, ag} = EstimatePrecisions @@ Join[VParams, {T, zd0, TDTmu}];
	eps = PrecisionEps[wp]/2;
	Integrand[kS_?NumberQ] := (
		N[($E[kS] - $E[0]) (($E[kS] - $E[0]) - xT / g Log[xzd0] + xTDTmu) / (xzd0^(-1) Exp[g ($E[kS] - $E[0]) / xT] - 1), wp]
	);

	FirstBandIntegrand[kS_?NumberQ] := (
		ekS0 = N[$E[SetPrecision[kS, Infinity]] - $E[0], wp];
		ekS0 = SetPrecision[ekS0, Accuracy@ekS0];
		If[g ekS0 / xT < eps,
			(* Case when exponential argument is very small, even less than the
			epsilon for precision wp, i.e., the next nearest representable number
			at precision wp. *)
			N[ekS0 (ekS0 - xT / g Log[xzd0] + xTDTmu) / (xzd0^(-1) + xzd0^(-1) g ekS0 / xT - 1), wp],

			(* Other cases... *)
			N[ekS0 (ekS0 - xT / g Log[xzd0] + xTDTmu) / (xzd0^(-1) Exp[g ekS0 / xT] - 1), wp]
		]
	);

	Return[
		Integrator[
			Integrand,
			ede0edmuTDTmuQExpd1Int::PrettyString,
			SetPrecision[#, Infinity] &/@ VParams,
			xT,
			xzd0,
			{wp, pg, ag},
			FirstBandIntegrand
		]
	];
];


edmuTDTmuLog1dExpInt::PrettyString =  "(\[Epsilon] - \[Mu] + T d\[Mu]/dT) Log[1 - Exp[-\[Gamma](\[Epsilon] - \[Mu])/T] - 1].";
edmuTDTmuLog1dExpInt[$E_, VParams_, T_, zd0_, TDTmu_] := Block[
	{a0, g, ekS0, xa0, xT, xzd0, xTDTmu,FirstBandIntegrand, Integrand, eps, wp, pg, ag},

	a0 = Last[VParams];
	{xa0, xT, xzd0, xTDTmu} = SetPrecision[#, Infinity]& /@ {a0, T, zd0, TDTmu};
	g = xa0^(-2) / (4 Pi);

	{wp, pg, ag} = EstimatePrecisions @@ Join[VParams, {T, zd0, TDTmu}];
	eps = PrecisionEps[wp]/2;
	Integrand[kS_?NumberQ] := (
		N[(($E[kS] - $E[0]) - xT / g Log[xzd0] + xTDTmu) Log[1 - xzd0 Exp[-g ($E[kS] - $E[0]) / xT]], wp]
	);

	FirstBandIntegrand[kS_?NumberQ] := (
		ekS0 = N[$E[SetPrecision[kS, Infinity]] - $E[0], wp];
		ekS0 = SetPrecision[ekS0, Accuracy@ekS0];
		If[g ekS0 / xT < eps,
			(* Case when exponential argument is very small, even less than the
			epsilon for precision wp, i.e., the next nearest representable number
			at precision wp. *)
			N[(ekS0 - xT / g Log[xzd0] + xTDTmu) Log[1 - xzd0 + xzd0 g ekS0 / xT], wp],

			(* Other cases... *)
			N[(ekS0 - xT / g Log[xzd0] + xTDTmu) Log[1 - xzd0 Exp[-g ekS0 / xT]], wp]
		]
	);

	Return[
		Integrator[
			Integrand,
			edmuTDTmuLog1dExpInt::PrettyString,
			SetPrecision[#, Infinity] &/@ VParams,
			xT,
			xzd0,
			{wp, pg, ag},
			FirstBandIntegrand
		]
	];
];


edmuQExpd1Int::PrettyString =  "(\[Epsilon] - \[Mu]) / (Exp[\[Gamma](\[Epsilon] - \[Mu])/T] - 1).";
edmuQExpd1Int[$E_, VParams_, T_, zd0_] := Block[
	{a0, g, ekS0, xa0, xT, xzd0, FirstBandIntegrand, Integrand, wp, pg, ag, eps},

	a0 = Last[VParams];
	{xa0, xT, xzd0} = SetPrecision[#, Infinity]& /@ {a0, T, zd0};
	g = xa0^(-2) / (4 Pi);

	{wp, pg, ag} = EstimatePrecisions @@ Join[VParams, {T, zd0}];
	eps = PrecisionEps[wp]/2;
	Integrand[kS_?NumberQ] := (
		N[(($E[kS] - $E[0]) - xT / g Log[xzd0]) / (xzd0^(-1) Exp[g ($E[kS] - $E[0]) / xT] - 1), wp]
	);

	FirstBandIntegrand[kS_?NumberQ] := (
		ekS0 = N[$E[SetPrecision[kS, Infinity]] - $E[0], wp];
		ekS0 = SetPrecision[ekS0, Accuracy@ekS0];
		If[g ekS0 / xT < eps,
			(* Case when exponential argument is very small, even less than the
			epsilon for precision wp, i.e., the next nearest representable number
			at precision wp. *)
			N[(ekS0 - xT / g Log[xzd0]) / (xzd0^(-1) + xzd0^(-1) g ekS0 / xT - 1), wp],

			(* Other cases... *)
			N[(ekS0 - xT / g Log[xzd0]) / (xzd0^(-1) Exp[g ekS0 / xT] - 1), wp]
		]
	);

	Return[
		Integrator[
			Integrand,
			edmuQExpd1Int::PrettyString,
			SetPrecision[#, Infinity] &/@ VParams,
			xT,
			xzd0,
			{wp, pg, ag},
			FirstBandIntegrand
		]
	];
];


OneQExpd1Int::PrettyString = "1 / (Exp[\[Gamma](\[Epsilon] - \[Mu])/T] - 1).";
OneQExpd1Int[$E_, VParams_, T_, zd0_] := Block[
	{a0, g, ekS0, xa0, xT, xzd0, FirstBandIntegrand, Integrand, wp, pg, ag, eps},

	a0 = Last[VParams];
	{xa0, xT, xzd0} = SetPrecision[#, Infinity]& /@ {a0, T, zd0};
	g = xa0^(-2) / (4 Pi);

	{wp, pg, ag} = EstimatePrecisions @@ Join[VParams, {T, zd0}];
	eps = PrecisionEps[wp]/2;
	Integrand[kS_?NumberQ] := N[1 / (xzd0^(-1) Exp[g ($E[kS] - $E[0]) / xT] - 1), wp];

	FirstBandIntegrand[kS_?NumberQ] := (
		ekS0 = N[$E[SetPrecision[kS, Infinity]] - $E[0], wp];
		ekS0 = SetPrecision[ekS0, Accuracy@ekS0];
		If[g ekS0 / xT < eps,
			(* Case when exponential argument is very small, even less than the
			epsilon for precision wp, i.e., the next nearest representable number
			at precision wp. *)
			N[1 / (xzd0^(-1) + xzd0^(-1) g ekS0 / xT - 1), wp],

			(* Other cases... *)
			N[1 / (xzd0^(-1) Exp[g ekS0 / xT] - 1), wp]
		]
	);

    Return[If[xzd0 == 1,
    	Infinity,

		Integrator[
			Integrand,
			OneQExpd1Int::PrettyString,
			SetPrecision[#, Infinity] &/@ VParams,
			xT,
			xzd0,
			{wp, pg, ag},
			FirstBandIntegrand
		]
	]];
];



End[];


EndPackage[];
