(* :Name: ThermodynamicQuadratic.m *)

(* :Title: ThermodynamicQuadratic *)

(* :Author: Omar Abel Rodríguez <oarodriguez@live.com.mx> *)

(* :Summary: Implements routines to find the thermodynamic properties of
a system of bosons subject to a Kronig-Penney potential, where their energy
in the transversal direction to the potential barriers is quadratic. *)

(* :Context: MultiSlabs`Infinite`Bosons`ThermodynamicQuadratic` *)

(* :Package Version: 1.0 *)

(* :Copyright: Copyright 2014, *)

(* :History: *)

(* :Keywords: Quantum systems *)

(* :Source: *)

(* :Warnings: *)



BeginPackage["MultiSlabs`Infinite`Bosons`ThermodynamicQuadratic`"];


ClearThemodynamicCache::usage = "ClearThemodynamicCache[] removes saved values for
long-lasting routines.";

CriticalTemperature::usage = "CriticalTemperature[u0, r, a0] returns the critical temperature of a bosonic system subject to a Kronig-Penney potential with a disperstion relation function given by ekS.";

DensityFunction::usage = "DensityFunction[u0, r, a0, T, zr] calculates the density funcion of a Kronig-Penney
potential in the zr value given.";

CondensedFraction::usage = "CondensedFraction[u0, r, a0, T] returns the fraction of total
bosons in condensed state for bosonic system subject to a Kronig-Penney potential at a given
tempereture T.";

RelativeFugacity::usage = "RelativeFugacity[u0, r, a0, T] returns the relative fugacity
Exp[-\[Gamma](\[Epsilon] - \[Mu])/T] of a bosonic system subject to a Kronig-Penney potential at a given
tempereture T.";

ChemicalPotential::usage = "ChemicalPotential[u0, r, a0, T] returns the chemical potential of
a bosonic system subject to a Kronig-Penney potential at a given temperature T.";

InternalEnergy::usage = "InternalEnergy[u0, r, a0, T] returns the internal energy per particle of
a bosonic system subject to a Kronig-Penney potential at a given tempereture T.";

SpecificHeat::usage = "SpecificHeat[u0, r, a0, T] returns the specific heat of
a bosonic system subject to a Kronig-Penney potential at a given tempereture T.";

IsobaricSpecificHeat::usage = "IsobaricSpecificHeat[u0, r, a0, T] returns the isobaric specific heat of
a bosonic system subject to a Kronig-Penney potential at a given tempereture T.";

ChemicalPotentialDerivative::usage = "ChemicalPotentialDerivative[u0, r, a0, T]";



Begin["`Private`"];

Get["MultiSlabs`MultiSlabs`"];
Get["MultiSlabs`Infinite`Bosons`QuadraticQuads`"];

Energy = MultiSlabs`Infinite`KronigPenney`Energy;
WaveFunction = MultiSlabs`Infinite`KronigPenney`WaveFunction;

(* "Definitions" for the cache symbols. *)
$CriticalTemperatureCache = Symbol["$CriticalTemperatureCache"];
$RelativeFugacityCache = Symbol["$RelativeFugacityCache"];


(* Deletes the cached energy values  *)
ClearThemodynamicCache[] := (
	Clear[$CriticalTemperatureCache];
	Clear[$RelativeFugacityCache];
);


ClearAll[CriticalTemperature];
SetAttributes[CriticalTemperature, Listable];
SetAttributes[CriticalTemperature, NumericFunction];
CriticalTemperature::Completed = "Critical temperature value is `4` for {u0=`1`, r=`2`, a0=`3`}.";
CriticalTemperature[u0_?NumericQ, r_?NumericQ, a0_?NumericQ] /; If[u0 == 0 || r == 0, True,
	If[InexactNumberQ[u0] || InexactNumberQ[r] || InexactNumberQ[a0], True, False]
] := Block[{$E, NumberFunctionEquation, wp, ag, pg, Tc, rv},

	(* Trivial case *)
	If[u0 == 0 || r == 0, Return[1]];	(* Exit *)

	rv = $CriticalTemperatureCache[u0, r, a0];
	If[NumericQ[rv],
		MessageDebug[CriticalTemperature::Completed, u0, r, a0, rv];
		Return[rv]
	];	(* Exit now. *)

	(* Energy function *)
	$E[kS_] := Energy[SetPrecision[u0, Infinity], SetPrecision[r, Infinity], kS];

	NumberFunctionEquation[T_?NumericQ] := 1 + T / (a0 Pi Zeta[3/2]) Log1dExpInt[$E, {u0, r, a0}, T, 1];

	{wp, pg, ag} = EstimatePrecisions[u0, r, a0];
	rv = Tc /. FindRoot[NumberFunctionEquation[Tc] == 0, {Tc, 1/2, 1},
		(*EvaluationMonitor :> Print["Tc: ", Tc , " F[Tc]", NumberFunctionEquation[Tc]],*)
		WorkingPrecision -> wp, AccuracyGoal -> ag, PrecisionGoal -> pg
	];

	$CriticalTemperatureCache[u0, r, a0] = rv;
	MessageDebug[CriticalTemperature::Completed, u0, r, a0, rv];

	Return[rv];
];


SetAttributes[DensityFunction, Listable];
SetAttributes[DensityFunction, NumericFunction];
Options[DensityFunction] = {WorkingPrecision -> $MachinePrecision,
	AccuracyGoal -> Automatic, PrecisionGoal -> Automatic};
DensityFunction[u0_, r_, a0_, T_, zr_, OptionsPattern[]] := Block[
	{xu0, xr, xa0, xzr, xT, xzd0, $E, $WF, f0, zd0, wp, ag, ago, pg, pgo, syshash,
	syscache, retval, EndLabel},

	wp = OptionValue[WorkingPrecision];
	ago = OptionValue[AccuracyGoal];
	pgo = OptionValue[PrecisionGoal];

	pg = If[SameQ[pgo, Automatic], wp/2, pgo];
	ag = If[SameQ[ago, Automatic], wp/2, ago];

	zd0 = RelativeFugacity[u0, r, a0, T, WorkingPrecision -> wp, AccuracyGoal -> ag,
		PrecisionGoal -> pg];
	f0 = CondensedFraction[u0, r, a0, T, WorkingPrecision -> wp, AccuracyGoal -> ag,
		PrecisionGoal -> pg];

	{xu0, xr, xa0, xzr, xT, xzd0} = SetPrecision[#, Infinity]& /@ {u0, r, a0, zr, T, zd0};

	(* Energy function *)
	$E[kS_] := Energy[xu0, xr, kS];
	$WF[kS_] := WaveFunction[xu0, xr, SetPrecision[kS, Infinity], xzr];

	retval = f0 N[Abs[$WF[0]]^2, wp] - 2 Sqrt[1 / (4 Pi xa0^2) / Pi] / Zeta[3/2] xT
		Log1dExpPsi2Int[$E, $WF, {xu0, xr, xa0}, xT, xzd0, wp, pg, ag];

	(*$DensityFunctionNormCache[syshash] = retval;*)

	Label[EndLabel];
	Return[retval];
];


ClearAll[CondensedFraction];
SetAttributes[CondensedFraction, Listable];
SetAttributes[CondensedFraction, NumericFunction];
CondensedFraction::Completed = "Condensed fraction is `5` for {u0=`1`, r=`2`, a0=`3`, T=`4`}.";
CondensedFraction[u0_?NumericQ, r_?NumericQ, a0_?NumericQ, T_?NumericQ] /; If[u0 == 0 || r == 0, True,
	If[InexactNumberQ[u0] || InexactNumberQ[r] || InexactNumberQ[a0] || InexactNumberQ[T], True, False]
] := Block[{$E, Tc, rv},

	Tc = CriticalTemperature[u0, r, a0];
	If[T >= Tc,
		MessageDebug[CondensedFraction::Completed, u0, r, a0, T, 0];
		Return[0]
	];

	(* Energy function *)
	$E[kS_] := Energy[SetPrecision[u0, Infinity], SetPrecision[r, Infinity], kS];

	rv = 1 + T / (a0 Pi Zeta[3/2]) Log1dExpInt[$E, {u0, r, a0}, T, 1];

	MessageDebug[CondensedFraction::Completed, u0, r, a0, T, rv];
	Return[rv];
];


ClearAll[RelativeFugacity];
SetAttributes[RelativeFugacity, Listable];
SetAttributes[RelativeFugacity, NumericFunction];
RelativeFugacity::Completed = "Relative fugacity value is `5` for {u0=`1`, r=`2`, a0=`3`, T=`4`}.";
RelativeFugacity[u0_?NumericQ, r_?NumericQ, a0_?NumericQ, T_?NumericQ] /; If[u0 == 0 || r == 0, True,
	If[InexactNumberQ[u0] || InexactNumberQ[r] || InexactNumberQ[a0] || InexactNumberQ[T], True, False]
] := Block[{$E, NumberFunctionEquation, Tc, zd0, wp, ag, pg, rv},

	Tc = CriticalTemperature[u0, r, a0];
	If[T <= Tc, Return[1]];

	rv = $RelativeFugacityCache[u0, r, a0, T];
	If[NumericQ[rv],
		MessageDebug[RelativeFugacity::Completed, u0, r, a0, T, rv];
		Return[rv]
	]; (* Exit now. *)

	(* Energy function *)
	$E[kS_] := Energy[SetPrecision[u0, Infinity], SetPrecision[r, Infinity], kS];

	NumberFunctionEquation[zd0_?NumericQ] := 1 + T / (a0 Pi Zeta[3/2]) Log1dExpInt[$E, {u0, r, a0}, T, zd0];

	{wp, pg, ag} = EstimatePrecisions[u0, r, a0, T];
	rv = zd0 /. FindRoot[NumberFunctionEquation[zd0] == 0, {zd0, 1, 0},
		(*EvaluationMonitor :> Print[{"zd0:", zd0}],*)
		WorkingPrecision -> wp, AccuracyGoal -> ag, PrecisionGoal -> pg,
		Method -> "Brent"
	];

	$RelativeFugacityCache[u0, r, a0, T] = rv;
	MessageDebug[RelativeFugacity::Completed, u0, r, a0, T, rv];
	Return[rv];
];


ClearAll[Fugacity];
SetAttributes[Fugacity, Listable];
SetAttributes[Fugacity, NumericFunction];
Fugacity::Completed = "Fugacity value is `5` for {u0=`1`, r=`2`, a0=`3`, T=`4`}.";
Fugacity[u0_?NumericQ, r_?NumericQ, a0_?NumericQ, T_?NumericQ] /; If[u0 == 0 || r == 0, True,
	If[InexactNumberQ[u0] || InexactNumberQ[r] || InexactNumberQ[a0] || InexactNumberQ[T], True, False]
] := Block[{$E, zd0, rv},

	zd0 = RelativeFugacity[u0, r, a0, T];

	rv = zd0 Exp[Energy[u0, r, 0] / (4 Pi a0^2 T)];
	MessageDebug[Fugacity::Completed, u0, r, a0, T, rv];
	Return[rv];
];


ClearAll[ChemicalPotential];
SetAttributes[ChemicalPotential, Listable];
SetAttributes[ChemicalPotential, NumericFunction];
ChemicalPotential::Completed = "Chemical potential value is `5` for {u0=`1`, r=`2`, a0=`3`, T=`4`}.";
ChemicalPotential[u0_?NumericQ, r_?NumericQ, a0_?NumericQ, T_?NumericQ] /; If[u0 == 0 || r == 0, True,
	If[InexactNumberQ[u0] || InexactNumberQ[r] || InexactNumberQ[a0] || InexactNumberQ[T], True, False]
] := Block[{rv},

	rv = Energy[u0, r, 0] + 4 Pi a0^2 T Log[RelativeFugacity[u0, r, a0, T]];
	MessageDebug[ChemicalPotential::Completed, u0, r, a0, T, rv];
	Return[rv];
];


ClearAll[InternalEnergy];
SetAttributes[InternalEnergy, Listable];
SetAttributes[InternalEnergy, NumericFunction];
InternalEnergy::Completed = "Internal energy value is `5` for {u0=`1`, r=`2`, a0=`3`, T=`4`}.";
InternalEnergy[u0_, r_, a0_, T_, u0_?NumericQ, r_?NumericQ, a0_?NumericQ, T_?NumericQ] /; If[u0 == 0 || r == 0, True,
	If[InexactNumberQ[u0] || InexactNumberQ[r] || InexactNumberQ[a0] || InexactNumberQ[T], True, False]
] := Block[{$E, zd0, quads, rv},

	(* Energy function *)
	$E[kS_] := Energy[SetPrecision[u0, Infinity], SetPrecision[r, Infinity], kS];

	zd0 = RelativeFugacity[u0, r, a0, T];
	quads = {
		ede0Log1dExpInt[$E, {u0, r, a0}, T, zd0],
		G2ExpInt[$E, {u0, r, a0}, T, zd0]
	};

	rv = Energy[u0, r, 0] + 1 / (Pi Zeta[3/2] a0) (- T quads[[1]] + (4 Pi a0^2)T^2 quads[[2]]);
	MessageDebug[InternalEnergy::Completed, u0, r, a0, T, rv];
	Return[rv];
];


ClearAll[IsochoricSpecificHeat];
SetAttributes[IsochoricSpecificHeat, Listable];
SetAttributes[IsochoricSpecificHeat, NumericFunction];
IsochoricSpecificHeat::Completed = "Specific heat value is `5` for {u0=`1`, r=`2`, a0=`3`, T=`4`}.";
IsochoricSpecificHeat[u0_?NumericQ, r_?NumericQ, a0_?NumericQ, T_?NumericQ] /; If[u0 == 0 || r == 0, True,
	If[InexactNumberQ[u0] || InexactNumberQ[r] || InexactNumberQ[a0] || InexactNumberQ[T], True, False]
] := Block[{$E, zd0, g, quads, rv, U, TDTmu},

	(* Energy function *)
	$E[kS_] := Energy[SetPrecision[u0, Infinity], SetPrecision[r, Infinity], kS];

	zd0 = RelativeFugacity[u0, r, a0, T];
	TDTmu = ChemicalPotentialDerivative[u0, r, a0, T];
	quads = {
		ede0Log1dExpInt[$E, {u0, r, a0}, T, zd0],
		G2ExpInt[$E, {u0, r, a0}, T, zd0],
		ede0edmuTDTmuQExpd1Int[$E, {u0, r, a0}, T, zd0, TDTmu],
		edmuTDTmuLog1dExpInt[$E, {u0, r, a0}, T, zd0, TDTmu]
	};

	g = 1 / (4 Pi a0^2);
	rv = 1 / (Pi Zeta[3/2] a0) (-g quads[[1]] + 2 T quads[[2]] + g^2 quads[[3]] / T - g quads[[4]]);

	(* Get internal energy and sow it *)
	(*U = 1 / (4 Pi xa0^2) N[Energy[u0, r, 0], wp] / xT + fcr (- 1 / (4 Pi xa0^2) nigs[[1]] + xT nigs[[2]]);*)
	U = Energy[u0, r, 0] + 1 / (Pi Zeta[3/2] a0) (-T quads[[1]] + T^2 quads[[2]] / g);
	Sow[U, "InternalEnergy"];		(* Emit internal energy *)

	MessageDebug[IsochoricSpecificHeat::Completed, u0, r, a0, T, rv];
	Return[rv];
];

(* Legacy definition *)
SpecificHeat = IsochoricSpecificHeat;


ClearAll[IsobaricSpecificHeat];
SetAttributes[IsobaricSpecificHeat, Listable];
SetAttributes[IsobaricSpecificHeat, NumericFunction];
IsobaricSpecificHeat::Completed = "IsobaricSpecific heat value is `5` for {u0=`1`, r=`2`, a0=`3`, T=`4`}.";
IsobaricSpecificHeat[u0_?NumericQ, r_?NumericQ, a0_?NumericQ, T_?NumericQ] /; If[u0 == 0 || r == 0, True,
	If[InexactNumberQ[u0] || InexactNumberQ[r] || InexactNumberQ[a0] || InexactNumberQ[T], True, False]
] := Block[{$E, zd0, g, fg, quads, quadsn, e0, zd, isothComp, isenComp, rv, U, Cv, Cp, TDTmu},

	(* Energy function *)
	$E[kS_] := Energy[SetPrecision[u0, Infinity], SetPrecision[r, Infinity], kS];

	zd0 = RelativeFugacity[u0, r, a0, T];
	quadsn = {
		edmuQExpd1Int[$E, {u0, r, a0}, T, zd0],
		OneQExpd1Int[$E, {u0, r, a0}, T, zd0],
		Log1dExpInt[$E, {u0, r, a0}, T, zd0],
		edmuTDTmuLog1dExpInt[$E, {u0, r, a0}, T, zd0, 0]
	};

	g = 1 / (4 Pi a0^2);
	fg = 1 / (Pi Zeta[3/2] a0);

	isothComp = - fg quadsn[[2]];
	Sow[isothComp, "IsothermalCompressibility"];  (* Emit isothermal compressibility *)

	TDTmu = - (1 + g fg quadsn[[1]]) / (g fg quadsn[[2]]);
	quads = {
		ede0Log1dExpInt[$E, {u0, r, a0}, T, zd0],
		G2ExpInt[$E, {u0, r, a0}, T, zd0],
		ede0edmuTDTmuQExpd1Int[$E, {u0, r, a0}, T, zd0, TDTmu],
		edmuTDTmuLog1dExpInt[$E, {u0, r, a0}, T, zd0, TDTmu]
	};

	Cv = fg (-g quads[[1]] + 2 T quads[[2]] + g^2 quads[[3]] / T - g quads[[4]]);
	Sow[Cv, "IsochoricSpecificHeat"];  (* Emit isochoric specific heat *)

	e0 = Energy[u0, r, 0];
	U = e0 + fg (- T quads[[1]] + T^2 quads[[2]] / g);
	Sow[U, "InternalEnergy"];  (* Emit internal energy per boson *)

	rv = Cv + isothComp T (2 fg T quads[[2]] - g fg quadsn[[4]] - TDTmu g fg quadsn[[3]])^2;

	(* Isentropic compressibility in both T < Tc and T > Tc regions with different equations *)
	isenComp = If[zd0 == 1,
		1 / (2 fg T quads[[2]] - g fg quadsn[[4]])^2 Cv / T,
		Cv / rv isothComp
	];
	Sow[isenComp, "IsentropicCompressibility"];  (* Emit isentropic compressibility *)

	MessageDebug[IsobaricSpecificHeat::Completed, u0, r, a0, T, rv];
	Return[rv];
];


ClearAll[IsothermalCompressibility];
SetAttributes[IsothermalCompressibility, Listable];
SetAttributes[IsothermalCompressibility, NumericFunction];
IsothermalCompressibility::Completed = "Isothermal Compressibility value is `5` for {u0=`1`, r=`2`, a0=`3`, T=`4`}.";
IsothermalCompressibility[u0_?NumericQ, r_?NumericQ, a0_?NumericQ, T_?NumericQ] /; If[u0 == 0 || r == 0, True,
	If[InexactNumberQ[u0] || InexactNumberQ[r] || InexactNumberQ[a0] || InexactNumberQ[T], True, False]
] := Block[{$E, zd0, rv},

	zd0 = RelativeFugacity[u0, r, a0, T];

	rv = - 1 / (Pi Zeta[3/2] a0) OneQExpd1Int[$E, {u0, r, a0}, T, zd0];
	MessageDebug[IsothermalCompressibility::Completed, u0, r, a0, T, rv];
	Return[rv];
];


ClearAll[IsentropicCompressibility];
SetAttributes[IsentropicCompressibility, Listable];
SetAttributes[IsentropicCompressibility, NumericFunction];
IsentropicCompressibility::Completed = "Isentropic Compressibility value is `5` for {u0=`1`, r=`2`, a0=`3`, T=`4`}.";
IsentropicCompressibility[u0_?NumericQ, r_?NumericQ, a0_?NumericQ, T_?NumericQ] /; If[u0 == 0 || r == 0, True,
	If[InexactNumberQ[u0] || InexactNumberQ[r] || InexactNumberQ[a0] || InexactNumberQ[T], True, False]
] := Block[{rv, props},

	{_, props} = Reap[
		IsobaricSpecificHeat[u0, r, a0, T],
	_, Rule];

	rv = "IsentropicCompressibility" /. props;
	MessageDebug[IsentropicCompressibility::Completed, u0, r, a0, T, rv];
	Return[rv];
];


ClearAll[ChemicalPotentialDerivative];
SetAttributes[ChemicalPotentialDerivative, Listable];
SetAttributes[ChemicalPotentialDerivative, NumericFunction];
ChemicalPotentialDerivative::Completed = "Chemical potential implicit derivative times temperature is `5` for {u0=`1`, r=`2`, a0=`3`, T=`4`}.";
ChemicalPotentialDerivative[u0_?NumericQ, r_?NumericQ, a0_?NumericQ, T_?NumericQ] /; If[u0 == 0 || r == 0, True,
	If[InexactNumberQ[u0] || InexactNumberQ[r] || InexactNumberQ[a0] || InexactNumberQ[T], True, False]
] := Block[{$E, zd0, g, fg, quads, rv},

	(* Energy function *)
	$E[kS_] := Energy[SetPrecision[u0, Infinity], SetPrecision[r, Infinity], kS];

	zd0 = RelativeFugacity[u0, r, a0, T];
	quads = {
		edmuQExpd1Int[$E, {u0, r, a0}, T, zd0],
		OneQExpd1Int[$E, {u0, r, a0}, T, zd0]
	};

	g = 1 / (4 Pi a0^2);
	fg = 2 g Sqrt[g / Pi] / Zeta[3 / 2];
	rv = - (1 + fg quads[[1]]) / (fg quads[[2]]);

	MessageDebug[ChemicalPotentialDerivative::Completed, u0, r, a0, T, rv];
	Return[rv];
];


End[];


EndPackage[];
