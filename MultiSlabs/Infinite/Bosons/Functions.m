(* :Name: Functions.m *)

(* :Title: Functions *)

(* :Author: Omar Abel Rodríguez <oarodriguez@live.com.mx> *)

(* :Summary: Implements routines to find the thermodynamic properties of
a system of bosons subject to a Kronig-Penney potential, where their energy
in the transversal direction to the potential barriers is quadratic. *)

(* :Context: MultiSlabs`Infinite`Bosons`Functions` *)

(* :Package Version: 1.0 *)

(* :Copyright: Copyright 2014, *)

(* :History: *)

(* :Keywords: Quantum systems *)

(* :Source: *)

(* :Warnings: *)


BeginPackage["MultiSlabs`Infinite`Bosons`Functions`"];


Integrator::usage = "Integrator[f, str, {u0, r, g, T, zd0}, {wp, pg, ag}] evaluates the
semi-infinite integral of the function f with the given parameters and precisions."


Begin["`Private`"];

Get["MultiSlabs`MultiSlabs`"];

CentralEnergies = MultiSlabs`Infinite`KronigPenney`CentralEnergies;
Energy = MultiSlabs`Infinite`KronigPenney`Energy;
EnergyDiracComb = MultiSlabs`Infinite`KronigPenney`EnergyDiracComb;


(*******************************************************************************

	Returns the number of bands where the integrations yield a significant
				result for a given working precision.

The rationale behind this function is based on the fact that all the integrals
that determine the thermodynamic properties depend on a term of the form
Exp[-beta (E[kz] - E[mu])]. When the argument of the exponential grows the
function goes to zero rapidly, and when the latter is less than the epsilon, i.e.,
the relative difference between two successive floating point numbers with a
precision wp, it becomes insignificant in arithmetic operations at that precision.
Supplying larger arguments to the exponential function will not change the final
result of an operation, and instead may cause other issues, e.g., that Mathematica
try to allocate too much memory because is trying to calculate some operation
with a very tiny number.

*******************************************************************************)
GetSignificantBands[u0_, r_, a0_, T_, zd0_, wp_] := Block[
	{eps, e0, eMaxSignificant, ab, centralEnergies, significantBands},

	eps = PrecisionEps[wp];
	e0 = N[Energy[u0, r, 0], wp];
	eMaxSignificant = e0 - If[zd0 != 0, 4 Pi a0^2 T (Log[eps] - Log[zd0]), 0];
	centralEnergies = CentralEnergies[u0, r, Infinity, eMaxSignificant];
	ab = Length[centralEnergies];

	significantBands = If[ab > 0 && Last[centralEnergies] < eMaxSignificant,
		Ceiling[Sqrt[eMaxSignificant - r/(1+r)u0] / Pi],
		Length[Select[centralEnergies, # <= eMaxSignificant &]]
	];
	Return[{
		If[ab == 0, 1, ab],
		If[significantBands == 0, 1, significantBands]
	}];
];

(* Returns the number of bands where the integrations yield a significant
result for a given working precision. This overloaded function works for a
Dirac comb potential. *)
GetSignificantBands[P0_, a0_, T_, zd0_, wp_] := Block[
	{eps, e0, eMaxSignificant, centralEnergies, significantBands},

	eps = PrecisionEps[wp];
	e0 = N[EnergyDiracComb[P0, a0, 0], wp];
	eMaxSignificant = e0 - If[zd0 != 0, 4 Pi a0^2 T (Log[eps] - Log[zd0]), 0];

	significantBands = Ceiling[Sqrt[eMaxSignificant] / Pi];

	Return[If[significantBands == 0, 1, significantBands]];
];


(* Function to add the integral of the second and subsequent bands.
TODO: Test if no extra precision is a harmless choice. *)
AddBandIntegration[F_, {j_, oldSum_}, {wp_, pg_, ag_}] := Block[
	{isb, $MaxExtraPrecision = 0},

	isb = NIntegrate[F[kS], {kS, j Pi, (j + 1) Pi},
		Method -> "GlobalAdaptive", WorkingPrecision -> wp,
		PrecisionGoal -> pg, AccuracyGoal -> ag
	];
	If[$DEBUG === True,
		Print["Band ", j+1, ": (", j, "\[Pi], ", (j+1), "\[Pi]), Integral: ", isb, ", New approx.: ", oldSum + isb];
	];
	{j + 1, oldSum + isb}
];


(* Integrate over the first band separately.
TODO: allow user to specify the maximum extra precision. *)
FirstBandIntegration[F_, {wp_, pg_, ag_}] := Block[
	{$MaxExtraPrecision = 10000},

	(***********************************************************************

								NOTES

	We have to split the integral in two intervals because the double
	exponential transformation algorithm with (0, Pi) as integration
	limits shows an unexpected behavior, where some sample points used to
	estimate the integral fall outside the interval (0, Pi); more specifically,
	they are greater than Pi. This, coupled with the fact that the
	energy spectrum is discontinuous, is a recipe to undesirable results:
	Mathematica might try to evaluate the exponential function with a very
	large argument, with a very high memory usage too.

	***********************************************************************)
	NIntegrate[F[kS], {kS, 0, Pi/2},
		Method -> "DoubleExponential", WorkingPrecision -> wp,
		PrecisionGoal -> pg, AccuracyGoal -> ag
	] +
	NIntegrate[F[kS], {kS, Pi/2, Pi},
		WorkingPrecision -> wp,
		PrecisionGoal -> pg, AccuracyGoal -> ag
	]
];


(* ************* *)
MonotonicDecreasingIntegration[F_, VParams_, T_, zd0_, {wp_, pg_, ag_}, Ffb_:None] := Block[
	{ab, mb, ssb, issb, ifb, isb, irb, $Ffb, KeepAddingBands},

	(* Keep adding band integrations while this functions yield True,
	i.e., while adding one band does change the result. *)
	KeepAddingBands[{_, i1_}, {_, i2_}] := (N[Abs[i1 - i2], {pg, ag}] != 0);

	(* How many bands, at the given precision, at most, we will need? *)
	{ab, mb} = GetSignificantBands @@ Join[VParams, {T, zd0, wp}];

	$Ffb = If[Ffb =!= None, Ffb, F];
	ifb = FirstBandIntegration[$Ffb, {wp, pg, ag}];

	(* %%%%%% *)
	(* Print["Integral over first band (0, \[Pi]): ", ifb]; *)

	(* Integrate over the other bands one by one. *)
	{ssb, issb} = NestWhile[AddBandIntegration[F, #, {wp, pg, ag}] &, {1, ifb}, KeepAddingBands, 2, ab-1];

	(* Integrate over the rest of the bands. *)
	irb = NIntegrate[F[kS], {kS, ssb Pi, 3 mb Pi},
		Method -> "GlobalAdaptive", WorkingPrecision -> 1.5 wp,
		PrecisionGoal -> pg, AccuracyGoal -> ag
	];

	(* %%%%%%% *)
	(* Print["Integral from ", ssb, "\[Pi] up to ", mb, "\[Pi]: ", irb]; *)

	Return[{mb, issb + irb}];
];


SemiInfiniteIntegration = MonotonicDecreasingIntegration;
(* MonotonicDecreasingIntegration = SemiInfiniteIntegration; *)


(* Low level generic integrator that logs messages. *)
Integrator::BandsUsed =  "Numerical integration required `1` bands to converge
to the prescribed accuracy for integrand `2`.";
Integrator[F_, prettyStr_, VParams_, T_, zd0_, {wp_, pg_, ag_}, Ffb_:None] := Block[
	{bandsUsed, integration},

	(***************************************************************************

								ATTENTION

	In order for this function to work properly all its numerical arguments must
	be supplied as exact numbers.

	***************************************************************************)

	{bandsUsed, integration} = SemiInfiniteIntegration[F, VParams, T, zd0, {wp, pg, ag}, Ffb];

	If[$DEBUG === True,
		Print[StringForm[Integrator::BandsUsed, bandsUsed, prettyStr]];
	];

	Return[integration];
];


End[];


EndPackage[];
