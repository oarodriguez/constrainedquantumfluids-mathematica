(* :Name: KronigPenney.m *)

(* :Title: KronigPenney *)

(* :Author: Omar Abel Rodríguez <oarodriguez@live.com.mx> *)

(* :Summary: Implements routines to find the energy spectrum and wave functions
of a system of particles subject to a Kronig-Penney potential. *)

(* :Context: MultiSlabs`Infinite`KronigPenney` *)

(* :Package Version: 1.0 *)

(* :Copyright: Copyright 2014, *)

(* :History: *)

(* :Keywords: Quantum systems *)

(* :Source: *)

(* :Warnings: *)



BeginPackage["MultiSlabs`Infinite`KronigPenney`"];


ClearEnergyCache::usage = "ClearEnergyCache[] removes the energy values saved
to accelerate computations.";

ClearWaveFunctionCache::usage = "ClearWaveFunctionCache[] deletes some values saved
to accelerate computations of the wave function.";

ImplicitEnergyRelation::usage = "ImplicitEnergyRelation[u0, r, e, kS] returns the value of the
dispertion relation for a particle subject to a Kronig-Penney potential";

CentralEnergies::usage = "CentralEnergies[u0, r] returns a list with the central
energies of a system subject to a Kronig-Penney potential.";

Energy::usage = "Energy[u0, r, kS] returns the energy of a particle subject to a
Kronig-Penney potential.";

EnergyDiracComb::usage = "EnergyDiracComb[u0, a0, kS] returns the energy of a particle subject to a
Dirac comb potential.";

WaveFunction::usage = "WaveFunction[u0, r, kS, zr] calculates the wave funcion of a Kronig-Penney
potential in the zr value given.";


(* Begin private context *)
Begin["`Private`"]

(* "Definitions" for the cache symbols. *)
$EnergyCache = Symbol["$EnergyCache"];
$WaveFunctionCache = Symbol["$WaveFunctionCache"];
$CentralEnergiesCache = Symbol["$CentralEnergiesCache"];

Get["MultiSlabs`MultiSlabs`"];


(* Deletes the cached energy values  *)
ClearEnergyCache[] := (
	Clear[$EnergyCache];
	Clear[$CentralEnergiesCache];
	Clear[$DiracCombEnergyCache];
);
ClearWaveFunctionCache[] := Clear[$WaveFunctionCache];


(* Returns the equation that relates energy and momentum *)
SetAttributes[ImplicitEnergyRelation, Listable];
ImplicitEnergyRelation[u0_, r_, e_, kS_] := If[e <= u0,
	If[e == 0,
		1/(2 (1 + r)) Sqrt[u0] Sinh[r/(1 + r) Sqrt[u0]] + Cosh[r/(1 + r) Sqrt[u0]] - Cos[kS],
		If[e == u0,
			-r Sqrt[u0]/(2 (1 + r)) Sin[Sqrt[u0]/(1 + r)] + Cos[Sqrt[u0]/(1 + r)] - Cos[kS],
			(u0 - 2 e)/(2 Sqrt[e] Sqrt[u0 - e]) Sinh[r/(1 + r) Sqrt[u0 - e]] Sin[1/(1 + r) Sqrt[e]]
			+ Cosh[r/(1 + r) Sqrt[u0 - e]] Cos[1/(1 + r) Sqrt[e]] - Cos[kS]
		]
	],
	(u0 - 2 e)/(2 Sqrt[e] Sqrt[e - u0]) Sin[r/(1 + r) Sqrt[e - u0]] Sin[1/(1 + r) Sqrt[e]]
	+ Cos[r/(1 + r) Sqrt[e - u0]] Cos[1/(1 + r) Sqrt[e]] - Cos[kS]
];


(* Returns the derivative of the equation that relates energy and momentum *)
SetAttributes[ImplicitEnergyRelationDerivative, Listable];
ImplicitEnergyRelationDerivative[u0_, r_, e_] := If[e == 0,
	(* Limit when e -> 0 *)
	-(r + 2)/(4 (1 + r)^2) Cosh[r/(1+r) Sqrt[u0]] - 1/(4 (1 + r)^2) (
		(2 r + 3)(1 + r) / Sqrt[u0] + (1/3) Sqrt[u0]/(1 + r)
	) Sinh[r/(1+r) Sqrt[u0]],

	(* Limit when e -> u0 *)
	If[e == u0,
		- r (1 + 2 r)/(4 (1 + r)^2) Cos[1/(1+r) Sqrt[u0]] - 1/(4 (1 + r)^2) (
			(2 + 3 r)(1 + r) / Sqrt[u0] - (1/3) r^3 Sqrt[u0]/(1 + r)
		) Sin[1/(1+r) Sqrt[u0]],

		(* Other energies. *)
		D[ImplicitEnergyRelation[u0, r, $e, Pi/2], $e] /. ($e -> e)
	]
];


(* Finds the central energies of each allowed band, for which the "far band"
aproximation is not valid *)
SetAttributes[CentralEnergies, NHoldAll];
Options[CentralEnergies] = {WorkingPrecision -> $MachinePrecision};
CentralEnergies[u0_?NumericQ, r_?NumericQ, nbmax_:10, emax_:Infinity, OptionsPattern[]] /;
	((IntegerQ[nbmax] || nbmax == Infinity) || (NumericQ[emax] || emax == Infinity)) := Block[
	{wp},

	(* TODO: return central energies with the specified precision. *)
	wp = OptionValue[WorkingPrecision];

	(* At this point the WorkingPrecision option has no effect, as it is not important
	to get the central energies, but this beheviour is intended to change in future
	revisions of the software.  *)
	Return[MachineCentralEnergies[u0, r, nbmax, emax]];
];


(* Finds the central energies of each allowed band with machine precision. *)
MachineCentralEnergies[u0_?NumericQ, r_?NumericQ, nbmax_:10, emax_:Infinity] /;
	((IntegerQ[nbmax] || nbmax == Infinity) || (NumericQ[emax] || emax == Infinity)) := Block[
	{RDR, eTanAsympt, eCotAsympt, FindEnergiesBelow, FindEnergyAbove, FindEnergiesAbove,
	ContinueSearch, diffApproxExact, wp, wpc, wpo, lce,  nb, nbmin, nbecut, nbu0, ecut,
	nbands, energies, energyToCont, ec, e1, e2, DR1, DR2, he, Np, bctr, tanAsymptCtr,
	cotAsymptCtr, tanAsymptDelta, cotAsymptDelta, minAsymptDelta, syscache},

	(********************************************************************)
	(******************* SOME INTERNAL FUNCTIONS ************************)

	(* Reduced implicit relation for band midpoint, i.e., kS = Pi/2 *)
	RDR[e_?NumericQ] := ImplicitEnergyRelation[u0, r, e, Pi/2];

	(* Find the energies where the asymptotes of Tan[r/(1+r) Sqrt[e - u0]] occur. *)
	eTanAsympt[n_] := If[n==0, u0, u0 + ((2 n - 1) (1 + r)/(2 r) Pi)^2];

	(* Find the energies where the asymptotes of Cot[1/(1+r) Sqrt[e]] occur. *)
	eCotAsympt[n_] := ((1 + r) n Pi)^2;

	diffApproxExact[nb0_, e_] := If[(e <= u0 || Length[energies] == 0) || Last[energies] <= u0,
		Infinity,
		Last[energies] - r u0 / (1+r) - (nb0 - 1/2)^2 Pi^2
	];

	(* Find the central energies for the bands bellow the potential level. *)
	FindEnergiesBelow[nlow_, nup_] := (ec /. FindRoot[RDR[ec] == 0,
		{ec, ((1 + r) (# - 1) Pi)^2, ((1 + r) # Pi)^2},
		WorkingPrecision -> $MachinePrecision, Method -> "Brent"
	]) & /@ Range[nlow, nup];

	(* Find a single central energy above the level of the potential. *)
	FindEnergyAbove[{nb_, e_}, h_] := (
		e2 = e + h;
		DR1 = RDR[e];
		DR2 = RDR[e2];

		(* One of these equalities must hold always. *)
		(* XXX: Check the validity of this assertion. *)
		tanAsymptCtr = If[e2 >= eTanAsympt[tanAsymptCtr], tanAsymptCtr + 1, tanAsymptCtr];
		cotAsymptCtr = If[e2 >= eCotAsympt[cotAsymptCtr], cotAsymptCtr + 1, cotAsymptCtr];

		If[DR1*DR2 <= 0,
			ec = ec /. FindRoot[RDR[ec] == 0, {ec, e, e2},
				WorkingPrecision -> $MachinePrecision, Method -> "Brent"
			];
			AppendTo[energies, ec];
			{nb + 1, e2},
			{nb, e2}
		]
	);

	(* Find central energies above the level of the potential *)
	FindEnergiesAbove[{nb0_, e_}] := (
		tanAsymptDelta = eTanAsympt[tanAsymptCtr] - eTanAsympt[tanAsymptCtr - 1];
		cotAsymptDelta = eCotAsympt[cotAsymptCtr] - eCotAsympt[cotAsymptCtr - 1];
		minAsymptDelta = Min[tanAsymptDelta, cotAsymptDelta];
		he = If[e < u0, u0 - e, minAsymptDelta] / Np;

		(* Go subinterval by subinterval *)
		NestWhile[FindEnergyAbove[#1, he] &,
			FindEnergyAbove[{nb0, e}, he], ContinueSearch, 1, Np-1]

		(* XXX: Maybe this check should be uncommented.
		tanAsymptCtr = If[minAsymptDelta == tanAsymptDelta, tanAsymptCtr+1, tanAsymptCtr];
		cotAsymptCtr = If[minAsymptDelta == cotAsymptDelta, cotAsymptCtr+1, cotAsymptCtr];*)
	);

	ContinueSearch[{nb_, e_}] := (e < emax && diffApproxExact[nb, e] > $AnomalousEnergyTolerance && nb < nbmax);
	(********************************************************************)
	(********************************************************************)

	(* Keep the case u0==0 or r==0 as separate, after all it's very cheap
	to calculate the central energies in this case. *)
	If[u0 == 0 || r == 0,
		nbands = Min[Floor[Sqrt[emax] / Pi], nbmax];
		energies = ((2# - 1)/2 Pi)^2 & /@ Range[1, nbands];
		Return[energies];
	];

	(* The cache is saved in a format ["precision"][u0, r] *)
	syscache = $CentralEnergiesCache[N[u0], N[r]];
	{energies, energyToCont, tanAsymptCtr, cotAsymptCtr} = If[ArrayQ[syscache], {
			"energies" /. syscache,
			"energyToCont" /. syscache,
			"tanAsymptCtr" /. syscache,
			"cotAsymptCtr" /. syscache
		}, {{}, 0, 1, 1}
	];

	(* emax = Infinity; *)
	Np = 8;
	ecut = Min[u0, emax];
	lce = Length[energies];

	(* Return energies up to the maximum value *)
	If[lce > 0 && Last[energies] >= emax,
		Return[Select[energies, # <= emax &, nbmax]]
	];

	(* Return energies up to the maximum band *)
	If[lce > 0 && lce >= nbmax,
		Return[energies[[;; nbmax]]]
	];

	(* How many bands there are below the level of the potential? *)
	(* The number of complete cycles for which we know where the energies are for e < u0. *)
	nbecut = Floor[Sqrt[ecut] / (Pi (1 + r))];
	nbu0 = Floor[Sqrt[u0] / (Pi (1 + r))];

	energies = If[lce < Min[nbecut, nbmax], Join[energies,
		FindEnergiesBelow[lce+1, Min[nbecut, nbmax]]], energies];
	lce = Length[energies];

	(* Now we proceed to find the rest of the energies above u0. *)
	tanAsymptCtr = If[lce > nbecut, tanAsymptCtr, 1]; (* Counter for asymptote number. *)
	cotAsymptCtr = If[lce > nbecut, cotAsymptCtr, nbecut+1]; (* Counter for asymptote number. *)
	energyToCont = If[lce > nbecut, energyToCont, ((1 + r) nbecut Pi)^2];

	(* Print["Starting central energiescalculations..."]; *)
	(* Print[{"nbecut:", nbecut, "nbu0:", nbu0, "lce:", lce}]; *)
	(* Print[{"ecut:", ecut, "nbmax:", nbmax, "emax:", emax}]; *)
	(* Print[{"Complete bands below u0:", energies[[;;Min[lce, nbu0]]]}]; *)
	(* Print[{"Bands extended from cache:", energies}]; *)
	(* Print["Parameters: energyToCont: ", 1.0 energyToCont, ", tanAsymptCtr: ", tanAsymptCtr, ", cotAsymptCtr: ", cotAsymptCtr]; *)

	{bctr, energyToCont} = If[energyToCont < emax && lce < nbmax,
		NestWhile[FindEnergiesAbove, FindEnergiesAbove[{lce, energyToCont}], ContinueSearch, 1],
		{lce, energyToCont}
	];

	syscache = {
		"energyToCont" -> energyToCont,
		"tanAsymptCtr" -> tanAsymptCtr,
		"cotAsymptCtr" -> cotAsymptCtr,
		"energies" -> energies
	};
	$CentralEnergiesCache[N[u0], N[r]] = syscache;

	Return[energies];
];


(* Find the energy of a particle as function of the momentum for the given
parameters for the potential u0 and r *)
ClearAll[Energy];
SetAttributes[Energy, Listable];
SetAttributes[Energy, NumericFunction];
Energy[u0_?NumericQ, r_?NumericQ, kS_?NumericQ] /; If[u0 == 0 || r == 0, True,
	If[InexactNumberQ[u0] || InexactNumberQ[r] || InexactNumberQ[kS],
	True, False]
] := Block[{RDR, DRDR, xu0, xr, xkS, energies, ej, wp, pg, ag, j, rv},

	(* Trivial case *)
	If[u0 == 0 || r == 0, Return[kS^2]]; (* Exit *)

	rv = $EnergyCache[u0, r, kS];
	If[NumericQ[rv], Return[rv]]; (* Exit now. *)

	{xu0, xr, xkS} = SetPrecision[#, Infinity] &/@ {u0, r, kS};
	RDR[ek_?MatrixQ] := {{ ImplicitEnergyRelation[xu0, xr, ek[[1, 1]], xkS] }};
	DRDR[ek_?MatrixQ] := {{ ImplicitEnergyRelationDerivative[xu0, xr, ek[[1, 1]]] }};

	{wp, pg, ag} = 2 EstimatePrecisions[u0, r, kS];
	j = 1 + Quotient[Abs[kS], Pi];
	energies = MachineCentralEnergies[u0, r, j];

	ej = If[j <= Length[energies],
		{{ energies[[j]] }},
		(********************************************************************

			For this case the "far band" approximation is valid, and the
			energy for the kS momentum is very close to kS^2 + r/(1+r) u0

		********************************************************************)
		{{ kS^2 + r/(1+r) u0 }}
	];
	rv = (e /. FindRoot[RDR[e], {e, ej},
		WorkingPrecision -> wp, PrecisionGoal -> pg, AccuracyGoal -> ag,
		Jacobian -> DRDR[e]
	])[[1, 1]];

	$EnergyCache[u0, r, kS] = rv;
	Return[rv];
];


(* Find the energy of a particle as function of the momentum for the given
parameters for the potential u0 and r *)
SetAttributes[EnergyDiracComb, Listable];
SetAttributes[EnergyDiracComb, NumericFunction];
EnergyDiracComb[P0_, a0_, kS_] /; If[P0 == 0, True,
	If[InexactNumberQ[P0] || InexactNumberQ[a0] || InexactNumberQ[kS],
	True, False]
] := Block[{RDR, xP0, xa0, xkS, wp, pg, ag, j, rv},

	(* Trivial case *)
	If[P0 == 0, Return[kS^2]]; (* Exit *)

	rv = $DiracCombEnergyCache[P0, a0, kS];
	If[NumericQ[rv], Return[rv]]; (* Exit now. *)

	{xP0, xa0, xkS} = SetPrecision[#, Infinity] &/@ {P0, a0, kS};
	RDR[ek_] := xP0 xa0 If[ek == 0, 1, Sin[Sqrt[ek]] / Sqrt[ek]] + Cos[Sqrt[ek]] - Cos[xkS];

	{wp, pg, ag} = 2 EstimatePrecisions[P0, a0, kS];
	j = 1 + Quotient[Abs[kS], Pi];
	rv = (e /. FindRoot[RDR[e], {e, ((j - 1/2) Pi)^2, (j Pi)^2},
		WorkingPrecision -> wp, PrecisionGoal -> pg, AccuracyGoal -> ag
	]);

	$DiracCombEnergyCache[P0, a0, kS] = rv;
	Return[rv];
];


(* Returns the wave function as function of the momentum and position for
the given parameters of the potential *)
SetAttributes[WaveFunction, Listable];
SetAttributes[WaveFunction, NumericFunction];
WaveFunction[u0_, r_, kvS_, zr_] /; If[u0 == 0 || r == 0, True,
	If[InexactNumberQ[u0] || InexactNumberQ[r] || InexactNumberQ[kvS] || InexactNumberQ[zr],
	True, False]
] := (* WaveFunction[u0, r, kvS, zr] = *) Block[
	{e, n, alpha1, beta1, beta2, kS, kpS, kbkp, kpbk, ke, ket, A, B, e1, e2,
	alpha, psik, normSquared, wp, pg, ag, rv},

	(* Free particle *)
	If[u0 == 0 || r == 0, Return[Exp[I kvS (zr + 1/2)]]];

	(* rv = $WaveFunctionCache[u0, r, kvS, zr]; *)
	(* If[NumericQ[rv], Return[rv]]; *)

	{wp, pg, ag} = 2 EstimatePrecisions[u0, r, kvS, zr];
	{xu0, xr, xkvS, xzr} = SetPrecision[#, Infinity] &/@ {u0, r, kvS, zr};

	(* See Quantum Mechanics, Eugene Merzbacher, 2nd edition p. 104 *)
	n = If[IntegerQ[xzr], Ceiling[xzr]+1, Ceiling[xzr + (1/2)xr/(1+xr)]];
	e = N[Energy[xu0, xr, xkvS], wp];
	kS = Sqrt[e];
	kpS = Sqrt[xu0 - e];
	(* Print[{u0, r, kvS, zr, e}]; *)

	kbkp = If[xu0 < e, -I Sqrt[e / (e - xu0)], Sqrt[e / (xu0 - e)]];
	kpbk = If[xu0 < e, I Sqrt[(e - xu0) / e], Sqrt[(xu0 - e) / e]];
	ke = kpbk - kbkp;
	ket = kpbk + kbkp;

	alpha1 = If[xu0 == e,
		Cos[kS xr/(1 + xr)] + (kS xr/(1 + xr) / 2) Sin[kS xr/(1 + xr)],
		Cosh[kpS xr/(1 + xr)] Cos[kS xr/(1 + xr)] - (ke / 2) Sinh[kpS xr/(1 + xr)] Sin[kS xr/(1 + xr)]
	];
	beta1 = If[xu0 == e,
		Sin[kS xr/(1 + xr)] - (kS xr/(1 + xr) / 2) Cos[kS xr/(1 + xr)],
		(ke / 2) Sinh[kpS xr/(1 + xr)] Cos[kS xr/(1 + xr)] + Cosh[kpS xr/(1 + xr)] Sin[kS xr/(1 + xr)]
	];
	beta2 = If[xu0 == e,
		kS xr/(1 + xr) / 2,
		ket / 2 Sinh[kpS xr/(1 + xr)]
	];
	alpha = alpha1 Sin[kS] - beta1 Cos[kS] - Sin[xkvS];

	e1 = (1/2) xr/(1+xr) - 1/2;
	e2 = (1/2) xr/(1+xr) - 1;
	A = If[xu0 == e,
		Exp[I n xkvS] (beta2 Exp[-I / 2 kS / (1 + xr)] + alpha Exp[I / 2 kS / (1 + xr)]),
		1 / 2 Exp[I n xkvS] ((1 + I kbkp) beta2 Exp[I kS e1 - kpS e2] +
			(1 - I kbkp) alpha Exp[-I kS e1 - kpS e2])
	];
	B = If[xu0 == e,
		0,
		1 / 2 Exp[I n xkvS] ((1 - I kbkp) beta2 Exp[I kS e1 + kpS e2] +
			(1 + I kbkp) alpha Exp[-I kS e1 + kpS e2])
	];

	psik = If[1/2 xr/(1 + xr) - 1 <=  xzr - n <=  -1/2 xr/(1 + xr),
		(* Valley portions of the potential. *)
		Exp[I n xkvS] (beta2 Exp[I kS (xzr - n + 1/2)] + alpha Exp[-I kS (xzr - n + 1/2)]),

		(* Hill portions *)
		If[xu0 == e, A, A Exp[kpS (xzr - n)] + B Exp[-kpS (xzr - n)]]
	];

	normSquared = (Abs[beta2]^2 + Abs[alpha]^2)(1 - xr/(1 + xr)) +
		(2 / kS) Sin[kS (1 - xr/(1+xr))] Re[Conjugate[alpha] beta2] +
		If[xu0 == e,
			xr / (1 + xr) Abs[A]^2,
			(Abs[A]^2 Exp[- 2 Re[kpS]] + Abs[B]^2 Exp[2 Re[kpS]]) If[xu0 > e,
				Sinh[Re[kpS] xr / (1+xr)] / Re[kpS],
				xr / (1 + xr)
			] +
		If[xu0 > e,
			2 xr / (1 + xr) Re[Conjugate[A] B],
			2 / Im[kpS] Re[Conjugate[A] B Exp[2 I Im[kpS]] Sin[Im[kpS] xr / (1 + xr)]]
			]
	];

	rv = Quiet[psik / Sqrt[normSquared], {Power::infy, Infinity::indet}];
	(* $WaveFunctionCache[u0, r, kvS, zr] = rv; *)

	(* Print[{kS, kpS}];
	Print[{alpha1, alpha, beta1, beta2}]; *)
	(* Print[{A, B, psik, normSquared, rv}]; *)
	Return[rv];
];


End[];
(* End private context *)

EndPackage[];
