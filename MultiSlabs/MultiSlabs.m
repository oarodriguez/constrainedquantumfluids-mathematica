(* :Name: MultiSlabs.m *)

(* :Title: MultiSlabs  *)

(* :Author: Omar Abel Rodríguez <oarodriguez@live.com.mx> *)

(* :Summary: *)

(* :Context: MultiSlabs` *)

(* :Package Version: 1.0 *)

(* :Copyright: Copyright 2014, *)

(* :History: *)

(* :Keywords: Bose Einstein Condensations *)

(* :Source: *)

(* :Warnings: *)


BeginPackage["MultiSlabs`"];



HashSignature::usage = "HashSignature[expr]";

If[$VersionNumber < 9,
	ArrayReshape::usage = "ArrayReshape[expr, newshape] changes the shape of an arbitrary
	dimension array"
];

ClearCache::usage = "ClearCache[] removes the cache used for computations.";


(* ********* FUNCTIONS THAT CACULATE INTEGRALS ************* *)

(*NumberFunctionIntegration::usage = "NumberFunctionIntegration[u0, r, \[Gamma], T, zd0]";

InternalEnergyIntegrations::usage = "InternalEnergyIntegrations[u0, r, \[Gamma], T, zd0]";

SpecificHeatIntegrations::usage = "SpecificHeatIntegrations[u0, r, \[Gamma], T, zd0, T d\[Mu]/dT]";

ChemicalPotentialDerivativeIntegrations::usage = "ChemicalPotentialDerivativeIntegrations[u0, r, \[Gamma], T, zd0]";*)


$DEBUG::usage = "Enable or disable logging.";

$MaxExponentialArgument::usage = "The maximum number to use as argument in the exponential
function in calculations.";

$AnomalousEnergyTolerance::usage = "The minimum tolerance to consider an energy as anomalous.";

EstimatePrecisions::usage = "EstimatePrecisions[n1, n2, ...] returns estimated, useful working precision, accuracy goal and precision goal from a sequence of numerical values.";

PrecisionEps::usage = "PrecisionEps[wp] gives the maximum relative error due to
rounding in floating point arithmetic, for numbers with decimal precision given by wp.";

MessageDebug::usage = "MessageDebug[Message, arg1, arg2, ...] prints an informative message in
Debug level..";


Begin["`Private`"];


$DEBUG = True;

$MaxExponentialArgument = -Log[$MinNumber]/(1024);

$AnomalousEnergyTolerance = Pi^2 / 10;



HashSignature[expr_,  meth_: "SHA1", base_: 16] := IntegerString[Hash[expr, meth], base];

If[$VersionNumber < 9,
	ArrayReshape[list_, newshape_] := First[Fold[Partition[#1, #2]&, Flatten[list], Reverse[newshape]]];
];


EstimatePrecisions[args__?NumericQ] := Block[{wp, pg, ag},
	(* Our criterion to select a working precision, precision goal and
	accuracy goal.

	NOTE: If kS is a zero arbitrary precision number we can only use its
	accuracy instead its precision, as this last is always zero.
	See http://reference.wolfram.com/mathematica/tutorial/ArbitraryPrecisionNumbers.html
	for more info. *)
	pg = Min @@ Join[Precision[If[# == 0, 0, #]] &/@ List@args] / 2;
	ag = Min @@ Join[Accuracy /@ List@args] / 2;
	wp = 2 Max[pg, ag];

	Return[{wp, pg, ag}];
];


MessageDebug[args__] := If[$DEBUG === True,
	Print[StringForm @@ List@args];
];


(* Get the epsilon for the given precision. *)
PrecisionEps[wp_] := Block[{l = 1},
	FixedPoint[(l++; N[1 + 2^-l, wp]) &, 1];
	N[2^-l, wp]
];


End[];


EndPackage[];
