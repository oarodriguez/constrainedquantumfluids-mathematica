[Constrained Quantum Fluids][1]
================================================================================

A collection of libraries to calculate thermodynamic properties of quantum
systems subject to spatial constrictions.


        .........       .........       .........       .........
        |       |       |       |       |       |       |       |
        |       |       |       |       |       |       |       |
        |       |       |       |       |       |       |       |
        |       |       |       |       |       |       |       |
        |       |       |       |       |       |       |       |
        |       |       |       |       |       |       |       |
        |       |       |       |       |       |       |       |
    ....|       |.......|       |.......|       |.......|       |.......


This repository contains several [Mathematica](http://www.wolfram.com/mathematica/)
importable packages.

```
SetDirectory[NotebookDirectory[]];
Needs["MultiSlabs`"];
Needs["Multilayers`"];
```

After executing this code the library routines are imported and ready to be
used.


Authors
--------------


**Omar Abel Rodríguez López**

- <https://github.com/oarodriguez>
- <https://bitbucket.org/oarodriguez>


[1]: https://bitbucket.org/oarodriguez/constrainedquantumfluids-mathematica
